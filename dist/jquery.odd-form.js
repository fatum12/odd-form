/**
 * OddLabs jQuery form plugin
 */

(function($) {
	'use strict';

	var defaults = {
		fields: {},
		placeholder: {},
		validation: {},
		ajax: {}
	};

	$.fn.oddForm = function(options) {
		options = $.extend(true, {}, defaults, options);

		this.each(function() {
			var $el = $(this);

			init($el, options);
		});
	};

	$.fn.oddForm.setDefaults = function(options) {
		$.extend(true, defaults, options);
	};

	function init($el, config) {
		var fNames,
			fieldSettings,
			fieldsArray,
			index,
			fieldName,
			$field,
			rules = {},
			messages = {};

		for (fNames in config.fields) {
			fieldSettings = $.extend({
				placeholder: null,
				mask: null,
				rules: null,
				messages: null
			}, config.fields[fNames]);

			fieldsArray = fNames.split(/\s+/);
			for (index in fieldsArray) {
				fieldName = fieldsArray[index];

				$field = $('[name="' + fieldName + '"]', $el);
				if (!$field.length) {
					continue;
				}


				if (fieldSettings.placeholder) {
					placeholder($field, fieldSettings.placeholder, config.placeholder);
				}
				if (fieldSettings.mask) {
					if ($.isArray(fieldSettings.mask)) {
						mask($field, fieldSettings.mask[0], fieldSettings.mask[1]);
					}
					else {
						mask($field, fieldSettings.mask);
					}
				}
				if (fieldSettings.rules) {
					rules[fieldName] = fieldSettings.rules;
					if (fieldSettings.messages) {
						messages[fieldName] = fieldSettings.messages;
					}
				}
			}
		}

		$el.validate($.extend(
			{},
			config.validation,
			{
				rules: rules,
				messages: messages
			}
		));

		if (config.ajax !== false) {
			$el.ajaxForm(config.ajax);
		}
	}

	var placeholderPolyfill = $.fn.placeholder ? function($el, options) { $el.placeholder(options); } : function() {};

	function placeholder($el, text, options) {
		$el.attr('placeholder', text);

		placeholderPolyfill($el, options);
	}

	function mask($el, template, options) {
		$el.mask(template, options);
	}

})(jQuery);