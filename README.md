# jQuery OddLabs form plugin

jQuery плагин для быстрого создания ajax форм с валидацией и масками. Является оберткой над
[jquery.maskedinput](https://github.com/digitalBush/jquery.maskedinput),
[jquery.validation](https://github.com/jzaefferer/jquery-validation),
[jquery.form](https://github.com/malsup/form/) и [jquery.placeholder](https://github.com/mathiasbynens/jquery-placeholder).

Демо: http://odd.su/projects/odd-form/

## Использование

Подключаем необходимые для работы js файлы:

```
#!html
<script src="/path/to/jquery-1.11.3.min.js"></script>
<script src="/path/to/jquery.placeholder.min.js"></script> <!-- опционально -->
<script src="/path/to/jquery.maskedinput.min.js"></script>
<script src="/path/to/jquery.validate.min.js"></script>
<script src="/path/to/jquery.form.min.js"></script>

<script src="/path/to/jquery.odd-form.js"></script>
```

Вызов плагина:

```
#!javascript
$('form').oddForm(options);
```

Объект `options` может содержать следующие параметры:

### fields

Описание полей формы в виде объекта

```
#!javascript
fields: {
	// ...
	// имя поля ввода (значение атрибута name)
	fieldName: {
		// правила валидации в формате jquery.validation
		rules: {
			required: true
		},
		// сообщения, показываемые при ошибке валидации
		messages: {
			required: 'This field is required'
		},
		// html5 placeholder
		placeholder: 'mail@example.com',
		// маска ввода в формате jquery.maskedinput
		mask: '+7 (999) 999-99-99'
	}
	// ...
}
```

### validation

[Параметры плагина jquery.validation](http://jqueryvalidation.org/validate)

### ajax

[Параметры плагина jquery.form](http://jquery.malsup.com/form/#options-object)

### placeholder

[Параметры плагина jquery.placeholder](https://github.com/mathiasbynens/jquery-placeholder)